import React, { createContext, useState } from "react";

const ProfileInfo = createContext();

const ProfileContext = ({children}) => {

    const [workout, setWorkout] = useState(0);      // workouts tot
    const [calories, setCalories] = useState(0);    // calories tot
    const [minutes, setMinutes] = useState(0);      // minutes tot

    return (
        <ProfileInfo.Provider
            value={{
                workout, setWorkout,
                calories, setCalories,
                minutes, setMinutes
            }}>
            {children}
        </ProfileInfo.Provider>
    )
}

export {ProfileContext, ProfileInfo}