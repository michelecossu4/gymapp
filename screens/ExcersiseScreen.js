/*  ExcersiseScreen

    Schermata esercizio di un workout
*/

import React, { useState, useContext } from "react";
import { StyleSheet, Image, Text, TouchableOpacity, View } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';

{/* Context */}
import { ProfileInfo } from "../Context";

const ExcersiseScreen = (props) => {

    {/* Navigation */ }
    const navigation = props.navigation

    {/* excersises */ }
    const excersises = props.route.params.excersises;
    
    const { 
        workout, setWorkout,
        calories, setCalories,
        minutes, setMinutes
    } = useContext(ProfileInfo)

    const [index, setIndex] = useState(0);
    const current = excersises[index]

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "white", alignItems: "center" }}>

            {/* Excersise image */}

            <Image source={{ uri: current.image }} style={{ width: "100%", height: 300, marginTop: 20 }} />

            {/* Excersise name */}

            <Text style={[styles.itemTextStyle, { marginTop: 20 }]}>{current.name}</Text>

            {/* Excersise sets */}

            <Text style={[styles.itemTextStyle, { marginTop: 10 }]}>x{current.sets}</Text>

            {/* Button done */}

            <TouchableOpacity
                style={styles.doneButton}
                onPress={() => (
                    setIndex(index + 1),
                    setWorkout(workout + 1),
                    setMinutes(minutes + 2),
                    setCalories(calories + 8.1),
                    index === excersises.length - 1 ? navigation.goBack() : navigation.navigate("RestScreen")
                )}>

                <Text style={styles.buttonTextStyle}>DONE</Text>

            </TouchableOpacity>

            {/* Button Prev & Skip  */}

            <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10 }}>

                <TouchableOpacity
                    style={[styles.otherButtons, index === 0 && { backgroundColor: "#D3D3D3" }]}
                    disabled={index === 0}
                    onPress={() => setIndex(index - 1)}>

                    <Text style={styles.buttonTextStyle}>PREV</Text>

                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.otherButtons}
                    onPress={() => (
                        setIndex(index + 1),
                        index === excersises.length - 1 ? navigation.goBack() : navigation.navigate("RestScreen")
                    )}>

                    <Text style={styles.buttonTextStyle}>SKIP</Text>

                </TouchableOpacity>

            </View>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    itemTextStyle: {
        color: "gray",
        fontWeight: "bold",
        fontSize: 30,
    },
    doneButton: {
        width: 140,
        height: 50,
        marginVertical: 40,
        backgroundColor: "#18B7EA",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        borderRadius: 10
    },
    otherButtons: {
        width: 120,
        height: 40,
        marginHorizontal: 10,
        backgroundColor: "#228B22",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        borderRadius: 10
    },
    buttonTextStyle: {
        color: "white",
        fontWeight: "bold",
        fontSize: 20
    }
})

export default ExcersiseScreen;