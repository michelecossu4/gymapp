/*  RestScreen

    Schermata di pausa tra un esercizio ed un altro: countdown
*/

import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet } from "react-native"

const RestScreen = (props) => {

    {/* Navigation */}
    const navigation = props.navigation

    const [timer, setTimer] = useState(3)

    useEffect(() => {

        let timeoutID = setTimeout(() => {

            if(timer === 1) {
                clearTimeout(timeoutID)
                navigation.goBack()
            } else {
                setTimer(timer - 1)
            }
    
        }, 2000)

    }, [])

    return(
        <View style={{ flex: 1, alignItems: "center", backgroundColor: "white" }}>

            {/* Title */}

            <View 
                style={styles.titleStyle}>

                <Text style={{ fontSize: 150, color: "#FF5F1F" }}>REST</Text>

            </View>

            {/* Time left */}

            <View style={{ marginTop: 50, alignItems: "center" }}>

                <Text style={{ color: "black", fontWeight: "bold", fontSize: 40 }}>Time left:</Text>

                <Text style={{ color: "black", fontWeight: "bold", fontSize: 70, marginTop: 20 }}>{timer}</Text>

            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    titleStyle: { 
        marginTop: 20, 
        backgroundColor: "#ADD8E6", 
        width: "100%", 
        height: 250, 
        alignItems: "center", 
        justifyContent: "center",
        shadowColor: 'black',
        elevation: 10
    }

})

export default RestScreen;