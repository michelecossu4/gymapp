import * as React from 'react';
import { StyleSheet, Text, Pressable, Linking } from 'react-native';
import { Camera, useCameraDevices } from 'react-native-vision-camera';
import { useScanBarcodes, BarcodeFormat } from 'vision-camera-code-scanner';
import { RNHoleView } from 'react-native-hole-view';

export default function QrCodeScanner() {

    const [hasPermission, setHasPermission] = React.useState(false);
    const devices = useCameraDevices(); // useCameraDevices to get a Camera device
    const device = devices.back;

    const [frameProcessor, barcodes] = useScanBarcodes([BarcodeFormat.QR_CODE]);
    const [barcode, setBarcode] = React.useState(''); // variabile in cui viene salvato il contenuto del qrCode
    const [isScanned, setIsScanned] = React.useState(false); // diventa true se viene scannerizzato un qrCode

    React.useEffect(() => {
        toggleActiveState();
        return () => {
            barcodes;
        };
    }, [barcodes]);

    // imposta isScanner a true
    const toggleActiveState = async () => {
        if (barcodes && barcodes.length > 0 && isScanned === false) {
            setIsScanned(true);
            barcodes.forEach(async (scannedBarcode) => {
                if (scannedBarcode.rawValue !== '') {
                    setBarcode(scannedBarcode.rawValue);
                }
            });
        }
    };

    React.useEffect(() => {
        (async () => {
            const status = await Camera.requestCameraPermission();
            setHasPermission(status === 'authorized');
        })();
    }, []);

    return (
        device != null &&
        hasPermission && (
            <>
                <Camera
                    style={StyleSheet.absoluteFill}
                    device={device}
                    isActive={!isScanned}
                    frameProcessor={frameProcessor}
                    frameProcessorFps={5}
                />
                <RNHoleView
                    holes={[
                        {
                            x: 60,
                            y: 200,
                            width: 240,
                            height: 240,
                            borderRadius: 10,
                        },
                    ]}
                    style={styles.rnholeView}
                />
                {barcode != "" &&
                    <Pressable style={{ top: 50, alignItems: "center" }} onPress={() => Linking.openURL(barcode)}>
                        <Text style={styles.barcodeTextURL}>{barcode}</Text>
                    </Pressable>
                }
            </>
        )
    );
}

const styles = StyleSheet.create({
    barcodeTextURL: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
    },
    rnholeView: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
});