/*  HomeScreen

    Schermata in cui si possono vedere le proprie informazioni sugli
    allenamenti effettuati (workouts fatti, calorie bruciate, minuti 
    effettuati), oltre a poter scegliere l'allenamento da eseguire.
*/

import React, { useContext } from "react";
import { StyleSheet, View, Text, ScrollView, Image } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';

{/* Components */ }
import SetStatusBar from "../components/StatusBar";
import FitnessCards from "../components/FitnessCards";

{/* Context */}
import { ProfileInfo } from "../Context";

const HomeScreen = () => {

    {/* ProfileInfo information */}
    const { workout, minutes, calories } = useContext(ProfileInfo);

    {/* Top Item setup */ }
    function topItem(title, value) {
        return (
            <View style={{ alignItems: "center" }}>

                <Text style={styles.TopItemTextStyle}>{title}</Text>

                <Text style={styles.TopItemTextStyle}>{value}</Text>

            </View>
        )
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>

            {/* StatusBar */}

            <SetStatusBar />

            {/* Blue Top View */}

            <View style={{ backgroundColor: "#18B7EA", flex: 1, marginTop: 20, borderRadius: 10 }}>

                {/* Title */}

                <Text style={{ color: "white", fontWeight: "bold", fontSize: 18, padding: 10 }}>Workout Home Page</Text>

                {/* Top Items */}

                <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 10, marginTop: 20 }}>

                    {topItem("WORKOUTS", workout)}

                    {topItem("CALORIES", calories)}

                    {topItem("MINUTES", minutes)}

                </View>

            </View>

            <View style={{ flex: 3, marginTop: -80 }}>

                {/* Principal Image and Workouts */}

                <ScrollView>

                    <Image
                        source={{ uri: principalImage }}
                        style={{
                            width: "90%", height: 160, alignSelf: "center", borderRadius: 20
                        }}
                    />

                    <FitnessCards />

                </ScrollView>

            </View>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    TopItemTextStyle: {
        color: "white",
        fontWeight: "bold",
        fontSize: 17
    }
})

export default HomeScreen;

const principalImage = "https://cdn-images.cure.fit/www-curefit-com/image/upload/c_fill,w_842,ar_1.2,q_auto:eco,dpr_2,f_auto,fl_progressive/image/test/sku-card-widget/gold2.png";
