/*  Workout

    Schermata in cui sono elencati gli esercizi del workout selezionato ed
    in cui e' possibile iniziare l'allenamento
*/

import React from "react";
import { StyleSheet, Image, TouchableOpacity, ScrollView, View, Pressable, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

const Workout = (props) => {

    {/* Navigation */ }
    const navigation = props.navigation

    {/* excersises */ }
    const excersises = props.route.params.excersises;

    {/* principalImage */ }
    const principalImage = props.route.params.workoutImage

    return (
        <ScrollView style={{ flex: 1, backgroundColor: "white" }}>

            {/* Principal Image */}

            <Image
                source={{ uri: principalImage }}
                style={{ width: "100%", height: 200, marginTop: 20 }}
            />

            {/* Back button */}

            <TouchableOpacity
                style={{ position: "absolute", left: 10, top: 30 }}
                onPress={() => navigation.goBack()}>
                <Icon name="arrow-back" color="white" size={24} />
            </TouchableOpacity>

            {/* excersises */}

            {excersises.map((item, index) => (
                <Pressable
                    key={index}
                    style={[
                        { flexDirection: "row" }, 
                        index != excersises.length - 1 && styles.divider]}>

                    <Image
                        style={{ width: 110, height: 110 }}
                        source={{ uri: item.image }}
                    />

                    <View style={{ justifyContent: "center", marginLeft: 20 }}>

                        <Text style={styles.itemTextStyle}>{item.name}</Text>
                        <Text style={styles.itemTextStyle}>x{item.sets}</Text>

                    </View>

                </Pressable>
            ))}

            {/* start button */}

            <TouchableOpacity 
                style={styles.startButton} 
                onPress={() => navigation.navigate("ExcersiseScreen", {
                    excersises: excersises
                })}>

                <Text style={{ color: "white", fontWeight: "bold" }}>START</Text>

            </TouchableOpacity>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    itemTextStyle: {
        color: "black",
        fontWeight: "bold",
        fontSize: 18
    },
    divider: {
        borderBottomColor: "#B6B6B4", 
        borderBottomWidth: 1
    },
    startButton: {
        width: 100, 
        height: 40, 
        marginVertical: 10,
        backgroundColor: "#18B7EA", 
        justifyContent: "center", 
        alignItems: "center", 
        alignSelf: "center",
        borderRadius: 10
    }
})

export default Workout;