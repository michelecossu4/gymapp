import * as React from 'react';

{/* Navigation packages */}
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

{/* Screens */ }
import Workout from './screens/Workout';
import ExcersiseScreen from './screens/ExcersiseScreen';
import RestScreen from './screens/RestScreen';

{/* Components */}
import Footer from './components/Footer';

const Stack = createNativeStackNavigator();

const Routes = () => {

  {/* headerShown: false => non si vede il nome dei Stack Screens come header */}
  const screenOptions = {
    headerShown: false
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='HomeScreen' screenOptions={screenOptions}> 
        <Stack.Screen name="HomeScreen" component={Footer} />
        <Stack.Screen name="Workout" component={Workout} />
        <Stack.Screen name="ExcersiseScreen" component={ExcersiseScreen} />
        <Stack.Screen name="RestScreen" component={RestScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;