import React from "react";
import { StyleSheet, Pressable, View, Image, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

{/* Navigation */ }
import { useNavigation } from "@react-navigation/native";

{/* Dati per l'applicazione */ }
import fitness from "./data/fitness";

const FitnessCards = () => {

    {/* Navigation */ }
    const navigation = useNavigation()

    return (
        <View>

            {fitness.map((item, index) => (
                <Pressable
                    key={index}
                    onPress={() => {
                        navigation.navigate("Workout", {
                            workoutImage: item.image,
                            excersises: item.excersises
                        })
                    }}
                    style={{ width: "100%", marginTop: 10 }}>

                    {/* Immagine */}

                    <Image
                        source={{ uri: item.image }}
                        style={{ width: "90%", height: 180, alignSelf: "center", borderRadius: 20 }} />

                    {/* Nome */}

                    <Text style={styles.TopItemTextStyle}>{item.name}</Text>

                    {/* Icona fulmine */}

                    <View style={{ position: "absolute", left: 30, bottom: 10, width: 30, height: 30 }}>
                        <Icon name="bolt" size={24} color="white" />
                    </View>

                </Pressable>
            ))}

        </View>
    )
}

const styles = StyleSheet.create({
    TopItemTextStyle: {
        position: "absolute",
        color: "white",
        fontWeight: "bold",
        left: 30,
        top: 10,
        fontSize: 17
    }

})

export default FitnessCards;