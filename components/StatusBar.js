import React from "react";
import { StatusBar } from "react-native";

const SetStatusBar = () => {
    return (
        <StatusBar backgroundColor="white" barStyle={"dark-content"} />
    )
}

export default SetStatusBar;