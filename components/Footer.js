import React from 'react';
import { View, Text } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

{/* Screens */}
import HomeScreen from '../screens/HomeScreen';
import QrCodeScanner from '../screens/QrCodeScanner';

function Impostazioni() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{color: "black"}}>Impostazioni!</Text>
      </View>
    );
}
  
function Resoconto() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{color: "black"}}>Resoconto!</Text>
      </View>
    );
}

const Tab = createBottomTabNavigator();

export default function Footer() {
    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false,
                tabBarActiveTintColor: "#18B7EA",
                tabBarInactiveTintColor: "gray"
            }}>
            <Tab.Screen 
                name="Allenamento"
                component={HomeScreen}
                options={{
                    tabBarIcon: ({focused}) => {
                        return (<MaterialIcons name="timer" size={25} color={focused ? "#18B7EA" : "gray"} />)
                    }}}
                />
            <Tab.Screen 
                name="Qr Code" 
                component={QrCodeScanner}
                options={{tabBarIcon: ({focused}) => (<MaterialIcons name="qr-code-2" size={25} color={focused ? "#18B7EA" : "gray"} />), 
                    }}
                />
            <Tab.Screen 
                name="Resoconto" 
                component={Resoconto} 
                options={{tabBarIcon: ({focused}) => (<MaterialIcons name="bar-chart" size={25} color={focused ? "#18B7EA" : "gray"} />)}}
                />
            <Tab.Screen 
                name="Impostazioni" 
                component={Impostazioni} 
                options={{tabBarIcon: ({focused}) => (<MaterialIcons name="person" size={25} color={focused ? "#18B7EA" : "gray"} />)}}
                />
        </Tab.Navigator>
    );
}

