import * as React from 'react';

{/* Context */}
import { ProfileContext } from './Context';

{/* Navigation File */}
import Routes from './Routes';

const App = () => {
  return (
    <ProfileContext>
      <Routes />
    </ProfileContext>
  );
};

export default App;
